package orange.ro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Calculation {
    public static void main(String[] args) {
        SpringApplication.run(Calculation.class, args);

    }
}
