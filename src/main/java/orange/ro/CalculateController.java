package orange.ro;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculateController {
    private int square, sum;

    @RequestMapping("/square")
    public String calculateSquare(@RequestParam("value") int value) {
        square = value * value;
        return ("Square of " + value + " is: " + square);
    }

    @RequestMapping("/sum")
    public String calculateSum(@RequestParam("int1") int int1, @RequestParam("int2") int int2) {
        sum = (int1 + int2);
        return ("Sum of " + int1 + " and " + int2 + "is: " + sum);
    }

 }
